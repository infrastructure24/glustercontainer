FROM debian:bullseye
# kernel in proxmox: Linux 5.13.19-2
# kernel in debian:bullseye 5.10
# https://en.wikipedia.org/wiki/Debian_version_history#Debian_11_(Bullseye)

# set timezone?

RUN apt -y update && apt -y upgrade
RUN apt -y install lvm2 glusterfs-server